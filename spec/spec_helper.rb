require 'rack/test'
require 'rspec'

ENV['RACK_ENV'] = 'test'

require File.expand_path '../../lib/hefservice.rb', __FILE__

module RSpecMixin
	include Rack::Test::Methods
	def app() Sinatra::Application end
end

RSpec.configure {|c| c.include RSpecMixin}
RSpec.configure do |c|
	c.color = true
	c.tty = true
	c.formatter = :progress
#	c.formatter = :documentation
end

