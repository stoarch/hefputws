require_relative  'spec_helper.rb'

describe 'Put web service app' do


	it 'should allow accessing the put count' do
		get '/puts?countOnly=true'

		last_response.should be_ok
		last_response.body.should match /{"put_count":\w+}/

	end


	it 'should get all put list' do
		get '/puts'


		last_response.should be_ok

		last_response.body.should =~ /(?:^\[|,)(?>({"put_id":\d+,"name":"(?:\w|\d)+","caption":"\b(?:(?:\w+(?:\s+|-)\w*)|(?:\w+))","active":\w+})(?:(?=(?:,{))|(?=\])))/
	end


	it 'should get put by no' do
		get '/puts/1' 

		last_response.should be_ok

		last_response.body.should =~ /(?>({"put_id":\d+,"name":"(?:\w|\d)+","caption":"\b(?:(?:\w+(?:\s+|-)\w*)|(?:\w+))","active":\w+}))/
	end


	it 'should get all put chanels' do
		get '/puts/1/chanels'

		last_response.should be_ok

		last_response.body.should =~ /{"put_id":\d+,"chanel_count":\d+,"chanels":\[{"chanel_id":\d+,"caption":"\w+","active":\w+}(?:,(?>{"chanel_id":\d+,"caption":"\w+","active":\w+}))*?\]}/ 
	end


	it 'should get count of put chanels' do
		get '/puts/1/chanels?countOnly=true'

		last_response.should be_ok
		last_response.body.should =~ /{"put_id":\d+,"chanel_count":\d+}/
	end


	it 'should get chanel by number' do
		get '/puts/1/chanels/1'

		last_response.should be_ok

		last_response.body.should =~ /{"put_id":\d+,"chanel_count":\d+,"chanel":{"chanel_id":\d+,"caption":"\w+","active":true}}/
	end


	it 'should get all put chanel values' do
		get '/puts/1/chanels/1/values' 

		last_response.should be_ok

		last_response.body.should =~ /(?>{"put":{"caption":"(?:[\w|\-|\d]+)","active":\w+,"id":\d+,"name":"(?:[\w|\d]+)","parallel":\w+,"vzljot_device_code":"\d+","order":\d+,"chanel_count":\d+},"chanel":{"caption":"[\w|\s]+","put_id":\d+,"active":true,"id":\d+,"put_ch_id":\d+},"temperature":{"chanel_id":\d+,"param_kind_id":\d+,"value":\d+\.\d+,"date_query":"\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2} \+0600","id":\d+,"valid":\w+},"pressure":{"chanel_id":\d+,"param_kind_id":\d+,"value":\d+\.\d+,"date_query":"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} \+0600","id":\d+,"valid":\w+},"flowrate":{"chanel_id":\d+,"param_kind_id":\d+,"value":\d+\.\d+,"date_query":"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} \+0600","id":\d+,"valid":\w+}})/
	end


	it 'should return one chanel value' do
		get '/puts/1/chanels/1/values/1'

		last_response.should be_ok
		store_response( last_response.body )
	end

	describe 'When no db exists' do
		before (:all) do
			output = Proc.new do |query|
				case query
				when /SELECT.*?count.*put/ then throw Sequel::DatabaseConnectionError.new() #items
				end
			end

			$db = Sequel.mock(fetch: output)
		end

		it 'should return put count 0' do
			get '/puts?countOnly=true'

			last_response.should be_ok
			last_response.body.should match /{"put_count":0}/

		end

	end

	## TOOLS ##
	###########

	def store_response( resp )
		open("resp.txt","w"){|f| f.puts resp }
	end

end
