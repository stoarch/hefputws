# encoding: utf-8
require 'sinatra'
require 'sinatra/reloader'
require 'sequel'
require 'json'
require 'ap'

set :port, 9000
#set :bind, '192.168.254.125'
set :bind, '185.48.149.157'


# return all put list
#
get '/puts' do

	if params["countOnly"] #need to return count
		db = connect_to_db()

		puts_count = db[:put].where(active: true).count

		db.disconnect

		res = {"put_count" => puts_count }

		status = 200
		body res.to_json 
	else # return all list
		db = connect_to_db()

		puts = db[:put].where(active: true).all

		db.disconnect

		
		res = []
		puts.each do |cur_put|
			res << {"put_id" => cur_put[:id], "name" => cur_put[:name], "caption" => cur_put[:caption], "active" => cur_put[:active]}
		end

		status = 200
		body res.to_json 
	end
end

# return put by index
# 
get '/puts/:no' do

	put_no = params[:no].to_i

	db = connect_to_db()

	puts = db[:put].where( active: true ).all
	cur_put = puts[put_no]

	db.disconnect

	
	res = {"put_id" => cur_put[:id], "name" => cur_put[:name], "caption" => cur_put[:caption], "active" => cur_put[:active]}

	status = 200
	body res.to_json 
end

# return put chanel by put_no (from 0 to max_put_count) and chanel
#
get '/puts/:put_no/chanels' do

	if params["countOnly"] # return count of items
		put_no = params[:put_no].to_i

		db = connect_to_db()

		puts = db[:put].where( active: true ).all
		cur_put = puts[put_no]

		chanel_count = db[:chanel].where( put_id: cur_put[:id]).count

		db.disconnect
		
		res = {"caption" => cur_put[:caption],"put_name" => cur_put[:name], "put_id" => cur_put[:id], "chanel_count" => chanel_count }

		status = 200
		body res.to_json 
	else

		put_no = params[:put_no].to_i
		chanel_no = params[:chanel_no].to_i

		db = connect_to_db()

		puts = db[:put].where( active: true).all
		cur_put = puts[put_no]

		chanels = db[:chanel].where( put_id: cur_put[:id]).all

		db.disconnect
		
		res = {"caption" => cur_put[:caption],"put_name" => cur_put[:name],"put_id" => cur_put[:id], "chanel_count" => chanels.size, "chanels" => []}

		chanels.each do |ch|
			res["chanels"] << { "chanel_id" => ch[:id], "caption" => ch[:caption], "active" => ch[:active]}
		end


		status = 200
		body res.to_json 
	end

end

# return put chanels count by put index (from start - unsorted)
# 
get '/puts/:no/chanels/count' do

	put_no = params[:no].to_i

	db = connect_to_db()

	puts = db[:put].where( active: true ).all
	cur_put = puts[put_no]

	chanel_count = db[:chanel].where( put_id: cur_put[:id]).count

	db.disconnect
	
	res = {"caption" => cur_put[:caption],"put_name" => cur_put[:name],"put_id" => cur_put[:id], "chanel_count" => chanel_count }

	status = 200
	body res.to_json 
end

# return put chanel by put_no (from 0 to max_put_count) and chanel
#
get '/puts/:put_no/chanels/:chanel_no' do

	put_no = params[:put_no].to_i
	chanel_no = params[:chanel_no].to_i

	db = connect_to_db()

	puts = db[:put].where( active: true ).all
	cur_put = puts[put_no]

	chanels = db[:chanel].where( put_id: cur_put[:id]).all

	if( chanel_no < 0 )||( chanel_no >= chanels.size )
		cur_chanel = { id: -1, caption: 'unknown', active: false }
	else
		cur_chanel = chanels[chanel_no]
	end

	db.disconnect
	
	res = {"caption" => cur_put[:caption],"put_name" => cur_put[:name],"put_id" => cur_put[:id], "chanel_count" => chanels.size,  "chanel" => 
		 { "chanel_id" => cur_chanel[:id], "caption" => cur_chanel[:caption], "active" => cur_chanel[:active]}
	}

	status = 200
	body res.to_json 

end


get '/puts/:put_no/values' do
	put_no = params["put_no"].to_i

	db = connect_to_db()

	puts = db[:put].where( active: true ).all

	if( put_no < 0 || put_no >= puts.size )
		status = 500
		body '{"put":{"caption":"unknown","active":false,"id":-1, "chanel_count":-1}'
		return 
	end

	put = puts[put_no]
	put_id = put[:id]

	chanels = db[:chanel].where( put_id: put_id ).all

	put["chanel_count"] = db[:chanel].where(put_id: put_id).count

	vals = db[:params].where( chanel_id: chanels.map{|v|v[:id]}).
		order( :id ).last(3*put["chanel_count"])

	db.disconnect

	values = vals.group_by{|v| v[:chanel_id]}

	res = {}
	res["put"] = put
	res["values"] = values 


	status = 200
	body res.to_json

end

TEMPERATURE_PARAM = 1
PRESSURE_PARAM = 2
FLOWRATE_PARAM = 3

get '/put/:put_no/archive/temperature/:date_start/:date_end' do

	put_no = params["put_no"].to_i
	date_start = params["date_start"]
	date_end = params["date_end"]

	db = connect_to_db()

	puts = db[:put].where( active: true ).all

	if( put_no < 0 || put_no >= puts.size )
		status = 500
		body '{"put":{"caption":"unknown","active":false,"id":-1, "chanel_count":-1}'
		return 
	end

	put = puts[put_no]
	put_id = put[:id]

	chanels = db[:chanel].where( put_id: put_id ).all


	vals = db[:params].where( chanel_id: chanels.map{|v|v[:id]})
			.where( param_kind_id: TEMPERATURE_PARAM )
			.where{ date_query >=  date_start }
			.where{ date_query <= date_end }
			.order( :id )
			.all

	db.disconnect

	res = {}
	res["put"] = put
	res["chanels"] = chanels
	res["values"] = vals


	status = 200
	body res.to_json
end

get '/put/:put_no/archive/pressure/:date_start/:date_end' do

	put_no = params["put_no"].to_i
	date_start = params["date_start"]
	date_end = params["date_end"]

	db = connect_to_db()

	puts = db[:put].where( active: true ).all

	if( put_no < 0 || put_no >= puts.size )
		status = 500
		body '{"put":{"caption":"unknown","active":false,"id":-1, "chanel_count":-1}'
		return 
	end

	put = puts[put_no]
	put_id = put[:id]

	chanels = db[:chanel].where( put_id: put_id ).all


	vals = db[:params].where( chanel_id: chanels.map{|v|v[:id]})
			.where( param_kind_id: PRESSURE_PARAM)
			.where{ date_query >=  date_start }
			.where{ date_query <= date_end }
			.order( :id )
			.all

	db.disconnect

	res = {}
	res["put"] = put
	res["chanels"] = chanels
	res["values"] = vals


	status = 200
	body res.to_json
end

get '/put/:put_no/archive/flowrate/:date_start/:date_end' do

	put_no = params["put_no"].to_i
	date_start = params["date_start"]
	date_end = params["date_end"]

	db = connect_to_db()

	puts = db[:put].where( active: true ).all

	if( put_no < 0 || put_no >= puts.size )
		status = 500
		body '{"put":{"caption":"unknown","active":false,"id":-1, "chanel_count":-1}'
		return 
	end

	put = puts[put_no]
	put_id = put[:id]

	chanels = db[:chanel].where( put_id: put_id ).all


	vals = db[:params].where( chanel_id: chanels.map{|v|v[:id]})
			.where( param_kind_id: FLOWRATE_PARAM)
			.where{ date_query >=  date_start }
			.where{ date_query <= date_end }
			.order( :id )
			.all

	db.disconnect

	res = {}
	res["put"] = put
	res["chanels"] = chanels
	res["values"] = vals


	status = 200
	body res.to_json
end

# return all values from put chanel 
#
get '/puts/:put_no/chanels/:chanel_no/values' do

	put_no = params["put_no"].to_i
	ch_no = params["chanel_no"].to_i

	db = connect_to_db()

	puts = db[:put].where( active: true ).all

	if( put_no < 0 || put_no >= puts.size )
		status = 500
		body '{"put":{"caption":"unknown","active":false,"id":-1, "chanel_count":-1}'
		return 
	end

	put = puts[put_no]
	put_id = put[:id]

	chanels = db[:chanel].where( put_id: put_id ).all

	if( ch_no < 0 || ch_no >= chanels.size )
		status = 501
		res = {}
		res["put"] = put
		res["chanel"] = {"caption"=>"unknown", "put_id"=>-1, "active"=>false, "id"=>-1, "put_ch_id"=>-1}
		body  res.to_json
		return
	end

	chanel = chanels[ch_no]

	ch_id = chanel[:id]

	put["chanel_count"] = db[:chanel].where(put_id: put_id).count

	vals = db[:params].where( chanel_id: ch_id).
		order( :id ).last(3)

	db.disconnect

	res = {}
	res["put"] = put
	res["chanel"] = chanel
	res["temperature"] = vals.select{|v| v[:param_kind_id] == 1}[0]
	res["pressure"] = vals.select{|v| v[:param_kind_id] == 2}[0]
	res["flowrate"] = vals.select{|v| v[:param_kind_id] == 3}[0]


	status = 200
	body res.to_json

end

# return value from put chanel 
#
get '/puts/:put_no/chanels/:chanel_no/values/:value_no' do

	put_no = params["put_no"].to_i
	ch_no = params["chanel_no"].to_i
	val_no = params["value_no"].to_i

	db = connect_to_db()

	puts = db[:put].where( active: true ).all

	if( put_no < 0 || put_no >= puts.size )
		status = 500
		body '{"put":{"caption":"unknown","active":false,"id":-1, "chanel_count":-1}'
		return 
	end

	put = puts[put_no]
	put_id = put[:id]

	chanels = db[:chanel].where( put_id: put_id ).all

	if( ch_no < 0 || ch_no >= chanels.size )
		status = 501
		res = {}
		res["put"] = put
		res["chanel"] = {"caption"=>"unknown", "put_id"=>-1, "active"=>false, "id"=>-1, "put_ch_id"=>-1}
		body  res.to_json
		return
	end

	chanel = chanels[ch_no]

	ch_id = chanel[:id]

	put["chanel_count"] = db[:chanel].where(put_id: put_id).count

	vals = db[:params].where( chanel_id: ch_id).
		order( :id ).last(3)

	db.disconnect

	res = {}
	res["put"] = put
	res["chanel"] = chanel

	if( val_no < 0 || val_no >= vals.size )
		res["value"] = {"chanel_id" => ch_id, "param_kind_id" => -1, "value" => -1, "date_query" => DateTime.now, "id" => -1, "valid" => false}
	else
		res["value"] = vals[val_no]
	end


	status = 200
	body res.to_json

end


## TOOLS ##
###########

def connect_to_db()
	$db ||=	Sequel.connect('postgres://postgres:1@192.168.254.125/ACSTP_HEF')

end
